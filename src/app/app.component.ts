import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Environment } from '@ionic-native/google-maps/ngx';
import { AuthService } from './services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Mapa',
      url: '/',
      auth: false,
      icon: 'map'
    },
    {
      title: 'Favoritos',
      url: '/list',
      auth: true,
      icon: 'star'
    },
    {
      title: 'Perfil',
      url: '/perfil',
      auth: true,
      icon: 'person'
    }
  ];

  constructor(
    private platform: Platform,
    public auth: AuthService,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {

      Environment.setEnv({
        // Api key for your server
        // (Make sure the api key should have Website restrictions for your website domain only)
        'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyCjcS_O2B2mrCfGWkjN0FV7e5QYhkDZRc4',

        // Api key for local development
        // (Make sure the api key should have Website restrictions for 'http://localhost' only)
        'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyCjcS_O2B2mrCfGWkjN0FV7e5QYhkDZRc4'
      });
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
