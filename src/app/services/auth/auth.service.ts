import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import * as firebase from 'firebase/app';
import { ToastController } from '@ionic/angular'
import AuthProvider = firebase.auth.AuthProvider;
import { map, take } from 'rxjs/operators';
import { reject } from 'q';

@Injectable()
export class AuthService {
	private userOn: firebase.User;
	private userData;
	private PATH = 'usuarios/'

	constructor(public afAuth: AngularFireAuth, 
				private db: AngularFireDatabase,
				public toastController: ToastController) {
		afAuth.authState.subscribe(user => {
			this.userOn = user;
			if(user !== null) {
				db.object(this.PATH+user.uid).snapshotChanges().subscribe( dataUser => {
					this.userData = { uid: user.uid, email: user.email, ...dataUser.payload.val() };
				} );
			}
		});
	}

	get authenticated(): boolean {
		return this.userOn !== null;
	}

	get user() {
		return this.userOn !== null ? this.userData : false;
	}

	public login(credentials) {
		return new Promise( async( resolve, reject ) => {
			try {
				const user = await this.afAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password);
				const userData = await this.getUser(user.user.uid);
				
				const toast = await this.toastController.create({
					message: 'Bem vindo '+ userData['nome'],
					animated: true,
					color: 'success',
					duration: 10000
				  });
				toast.present();
				return resolve(this.user);
			} catch (error) {
				const toast = await this.toastController.create({
					header: 'Erro!',
					message: error,
					animated: true,
					color: 'danger',
					duration: 2000
				  });
				toast.present();
				return reject(error);
			}
		} );
	}

	public reAuth(old_password) {
		return new Promise( async( resolve, reject ) => {
			try {
				var credentials = firebase.auth.EmailAuthProvider.credential(this.user.email, old_password);
				await this.afAuth.auth.currentUser.reauthenticateWithCredential(credentials);
				return resolve(this.user);
			} catch (error) {
				const toast = await this.toastController.create({
					header: 'Erro!',
					message: error,
					animated: true,
					color: 'danger',
					duration: 2000
				  });
				toast.present();
				return reject(error);
			}
		} );
	}
  
	public signUp(credentials, info) {
		return new Promise( async ( resolve, reject ) => {
			try {
				const dataUser =  await this.afAuth.auth.createUserWithEmailAndPassword( credentials.email, credentials.password );
				await this.db.list(this.PATH).set(dataUser.user.uid, info);
				await this.login(credentials);
				return resolve( this.user );
			} catch (error) {				
				return reject ( error );
			}
		} );
	}

	public updatePerfil(credentials, info) {
		return new Promise( async ( resolve, reject ) => {
			try {
				if(credentials.email != this.user.email) {
					await this.afAuth.auth.currentUser.updateEmail(credentials.email);
					this.user.email = credentials.email;
				}
				if(credentials.password) {
					await this.afAuth.auth.currentUser.updatePassword(credentials.password);
				}
				await this.db.object(this.PATH+this.user.uid)
				.update(info);
				const toast = await this.toastController.create({
					message: 'Perfil atualizado com sucesso',
					animated: true,
					color: 'success',
					duration: 10000
				  });
				toast.present();
				return resolve( this.user );
			} catch (error) {				
				return reject ( error );
			}
		} );
	}

	async signOut() {
		try {
			await this.afAuth.auth.signOut();
		} catch (error) {
			console.log(error);
		}
	}

	getUser(uid):Promise<Object> {
		return new Promise ( (resolve,reject) =>{
			this.db.object(this.PATH+uid)
			.snapshotChanges().forEach( user =>{ 
				return resolve( user.payload.val() );
			});
		} );
	}

	isFavorito(idLocal) {
		return this.user.favoritos && this.user.favoritos[idLocal];
	}

	addFavorito(idLocal) {	
		if(this.user.favoritos) {
			this.user.favoritos[idLocal] = !this.isFavorito(idLocal);
			this.db.object(this.PATH+this.user.uid)
			.update({
				favoritos: this.user.favoritos
			});
		} else {
			let objFav = {};
			objFav[idLocal] = true;
			this.db.object(this.PATH+this.user.uid)
			.update({
				favoritos: objFav
			});
		}
	}

}