import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { GeoFire } from "geofire";
import { BehaviorSubject } from 'rxjs';
import { Events } from '@ionic/angular';
@Injectable({
  providedIn: 'root'
})
export class GeofireService {

  dbRef: any;
  geoFire: any;
  PATH = '/lugares/';
  PATH_LOCATIONS = '/locations/';

  hits = new BehaviorSubject([])

  constructor(private db: AngularFireDatabase,
              public events: Events) {
    /// Reference database location for GeoFire
    this.dbRef = this.db.list('/locations');
    this.geoFire = new GeoFire(this.db.database.ref('locations'));
  }

  /// Adds GeoFire data to database
  setLocation(key:string, coords: Array<number>) {
    this.geoFire.set(key, coords)
        .then(_ => console.log('location updated'))
        .catch(err => console.log(err))
  }

  calcDistance(partida, destino) {
    return GeoFire.distance(partida,destino);
  }

   /// Queries database for nearby locations
   /// Maps results to the hits BehaviorSubject
  getLocations(radius: number, coords: Array<number>, tipos:Array<string> = null) {
    this.geoFire.query({
      center: coords,
      radius: radius
    })
    .on('ready', () => {});
    this.geoFire.query({
      center: coords,
      radius: radius
    }).on('key_entered', (key, location, distance) => {
      this.getPlaceObj(key).then( place => {
        if(!place['denuncias'] || Object.keys(place['denuncias']).length < 3) {    
          if( !tipos || tipos.length == 0 || tipos.includes(place['tipo']) ) {
            this.events.publish('add_point', {key,location,distance});  
          }
        }
      });
    });
    this.geoFire.query({
      center: coords,
      radius: radius
    }).on('key_exited', (key, location, distance) => {
      this.getPlaceObj(key).then( place => {
        if(!place['denuncias'] || Object.keys(place['denuncias']).length < 3) {    
          if( !tipos || tipos.length == 0 || tipos.includes(place['tipo']) ) {
            this.events.publish('add_point', {key,location,distance});  
          }
        }
      });
    });
    this.geoFire.query({
      center: coords,
      radius: radius
    }).on('key_moved', (key, location, distance) => {
      this.getPlaceObj(key).then( place => {
        if(!place['denuncias'] || Object.keys(place['denuncias']).length < 3) {    
          if( !tipos || tipos.length == 0 || tipos.includes(place['tipo']) ) {
            this.events.publish('add_point', {key,location,distance});  
          }
        }
      });
    });
    
  }

  async getPlace(key) {
    const place = await this.getPlaceObj(key);
    place['media_nota'] = this.mediaNota(place);
    const location = await this.getLocationsObj(key);
    return {key: key,...place,...location};
  }

  getPlaceObj(key):Promise<Object> {
    return new Promise( async( resolve, reject ) => {
      this.db.object(this.PATH+key)
      .snapshotChanges().forEach( place =>{ 
        return resolve(place.payload.val());
      });
    });
  }

  getLocationsObj(key):Promise<Object> {
    return new Promise( async( resolve, reject ) => {
      this.db.object(this.PATH_LOCATIONS+key)
      .snapshotChanges().forEach( place =>{ 
        return resolve(place.payload.val());
      });
    });
  }

  async addPlace(data) {
    const place = await this.db.list(this.PATH).push(data);
    return place.key;    
  }

	getAvaliacao(uid, place) {
		return place.avaliacoes ? place.avaliacoes[uid] ? place.avaliacoes[uid] : 0 : 0;
	}

  mediaNota(place) {
    return place.nota ? place.nota / Object.keys(place.avaliacoes).length : 0;
  }

	avaliarLocal(uid, nota, place) {	
		if(place.avaliacoes) {   
      place.nota = place.nota + (nota-this.getAvaliacao(uid, place));
      place.avaliacoes[uid] = nota;      
			this.db.object(this.PATH+place.key)
			.update({
        nota: place.nota,
				avaliacoes: place.avaliacoes
      });
      place['media_nota'] = this.mediaNota(place);
      return place;
		} else {
			let avaliacoesObj = {};
      avaliacoesObj[uid] = nota;
      place.avaliacoes = avaliacoesObj;      
			this.db.object(this.PATH+place.key)
			.update({
        nota,
				avaliacoes: avaliacoesObj
      });      
      place['nota'] = nota;
      place['media_nota'] = nota;
      return place;
		}
  }

	isDenunciado(uid, place) {
		return place.denuncias && place.denuncias[uid];
  }
  
	denunciarLocal(uid, denuncia, place) {	
		if(place.denuncias) {   
      place.denuncias[uid] = denuncia;      
			this.db.object(this.PATH+place.key)
			.update({
				denuncias: place.denuncias
      });
      return place;
		} else {
			let denuncias = {};
      denuncias[uid] = denuncia;
      place.denuncias = denuncias;      
			this.db.object(this.PATH+place.key)
			.update({
				denuncias: denuncias
      });      
      return place;
		}
	}
}