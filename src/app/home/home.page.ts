import { Component, ViewChild } from '@angular/core';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment,
  MyLocation,
  GoogleMapsAnimation,
  ILatLng,
  BaseArrayClass
} from '@ionic-native/google-maps/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Platform, ToastController, LoadingController, ModalController, AlertController, Events, IonSelect } from '@ionic/angular';
import { LoginComponent } from '../components/login/login.component';
import { SignupComponent } from '../components/signup/signup.component';
import { AuthService } from '../services/auth/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { GeofireService } from '../services/geofire/geofire.service';
import { AddPlaceComponent } from '../components/add-place/add-place.component';
import { ShowPlaceComponent } from '../components/show-place/show-place.component';
declare var google: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [AuthService, GeofireService]
})
export class HomePage {
  map: GoogleMap = null;

  @ViewChild('select_tipo', {static: false}) select: IonSelect;
  loading: any;
  GoogleAutocomplete:any;
  autocomplete:any;
  position: any;
  location: any;
  geocoder:any;
  markers:any;
  autocompleteItems:any;
  loadingPlaces = false;
  tipos: any;
  

  constructor(
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public modalController: ModalController,
    public auth: AuthService,
    public toastController: ToastController,
    public geo: GeofireService,
    public events: Events,
    private platform: Platform,
    private alertController: AlertController,
    private geolocation: Geolocation,
    private db: AngularFireDatabase) {
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.autocomplete = { input: '' };
    this.autocompleteItems = [];
    this.geocoder = new google.maps.Geocoder;
    this.markers = [];
  }

  /**
   * Atualiza os itens da pesquisa de lugares
   * 
   */
  updateSearchResults(){
    console.log(this.autocomplete);
    this.loadingPlaces = true;
    
    if (this.autocomplete.input == '') {
      this.loadingPlaces = false;
      this.autocompleteItems = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions(
      { input: this.autocomplete.input },
      (predictions, status) => {
        this.autocompleteItems = [];
        predictions.forEach((prediction) => {
          this.autocompleteItems.push(prediction);
        });
        this.loadingPlaces = false;
    });
  }

  /**
   * Disparado quando clicado em um lugar da lista de pesquisa de lugares
   * 
   * @param item 
   */
  selectSearchResult(item){
    this.autocompleteItems = [];
  
    this.geocoder.geocode({'placeId': item.place_id}, async (results, status) => {
      if(status === 'OK' && results[0]){
        let position = {
            lat: results[0].geometry.location.lat(),
            lng: results[0].geometry.location.lng()
        };
        console.log(results[0].address_components);
				const toast = await this.toastController.create({
					header: results[0].address_components[0].long_name+'-'+results[0].address_components[2].short_name,
					animated: true,
          color: 'secondary',
          duration: 5000,
          cssClass: 'ion-text-center'
				  });
        toast.present();
        this.map.clear();
        this.map.setCameraTarget(position);
        this.location = { latitude: results[0].geometry.location.lat(), longitude: results[0].geometry.location.lng()};
        await this.geo.getLocations(50, [results[0].geometry.location.lat(), results[0].geometry.location.lng()], this.tipos);
      }
    })
  }

  ngOnDestroy() {    
    this.events.unsubscribe('add_point');
    this.events.unsubscribe('show_point');
    this.map.destroy();
    
  }

  async ngOnInit() {
    await this.platform.ready();
    this.events.subscribe('show_point', async (key) => {
      const place = await this.geo.getPlace(key);
      place['distancia'] = this.geo.calcDistance([this.position.latitude, this.position.longitude], place['l'])
      console.log(place);
      
      const modal = await this.modalController.create({
        component: ShowPlaceComponent,
        componentProps: {place},
      });
      await modal.present();
      modal.onWillDismiss()
      .then(data => {
        if(data) {
        this.selectTipo();
        }
      });
      return;
    });
    this.events.subscribe('add_point', (data) => {      
      const dataMarker:MarkerOptions = {
        title: data.key,
        position: {
          lat: data.location[0],
          lng: data.location[1]
        },
        
        icon: { 
          url: 'https://img.pngio.com/map-marker-pin-png-image-background-png-arts-pins-on-a-map-png-1000_1534.png',
          size:{
            width: 28,
            height: 40
          },
        },
        disableAutoPan: true,
      };
      let marker: Marker = this.map.addMarkerSync(dataMarker);

      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(data => {
        let marker: Marker = <Marker>data[1];
        marker.hideInfoWindow();
        console.log('b');
        console.log(marker.getTitle());
        
        if(marker.getTitle() == 'center') return;
        this.events.publish('show_point', marker.getTitle());  
      });
      marker.on(GoogleMapsEvent.INFO_CLICK).subscribe(data=>{
        let marker: Marker = <Marker>data[1];
        marker.hideInfoWindow();
      });
    });
    
    const cords = await this.geolocation.getCurrentPosition();
    this.position = cords.coords;
    this.location = cords.coords;

    const input = document.getElementById('pac-input');
    const searchBox = new google.maps.places.SearchBox(input);

    await this.loadMap(cords.coords);
  }

  async selectTipo() {
    this.map.clear()
    await this.geo.getLocations(50, [this.location.latitude, this.location.longitude], this.tipos);
  }

  /**
   * 
   * Carrega o mapa
   * 
   * @param cords coordenada do dispositivo
   */
  async loadMap(cords) {
    if(this.map == null) {
      this.map = GoogleMaps.create('map_canvas', {
        camera: { 
          target: {
            lat: cords.latitude,
            lng: cords.longitude
          },
          zoom: 15
        }
      });
    }

    this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe((data) => {
      this.addPlace(data);
    });
    await this.geo.getLocations(50, [cords.latitude, cords.longitude], this.tipos);
  }

  async addPlace(data) {
    if(!this.auth.authenticated) {
      return;
    }
    const alert = await this.alertController.create({
      header: 'Adicionar Local',
      message: 'Você deseja adicionar este local ao mapa?',
      buttons: [
        {
          text: 'Não',
          handler: () => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Sim',
          handler: async () => {
            console.log('Confirm Okay');
            const modal = await this.modalController.create({
              component: AddPlaceComponent,
              componentProps: {
                coords: data[0]
              }
            });
            return await modal.present();
          }
        }
      ]
    });

    await alert.present();
  }

  onMarkerClick(params: any) {
    let marker: Marker = <Marker>params[1];
    marker.hideInfoWindow();
    console.log(marker.getTitle());
    this.events.publish('show_point', marker.getTitle());  
    
  }

  async onButtonClick() {
    this.map.clear();

    this.loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    await this.loading.present();

    // Get the location of you
    this.map.getMyLocation().then((location: MyLocation) => {
      this.loading.dismiss();
      console.log(JSON.stringify(location, null ,2));

      // Move the map camera to the location with animation
      this.map.animateCamera({
        target: location.latLng,
        zoom: 10,
        tilt: 30
      });

      // add a marker
      let marker: Marker = this.map.addMarkerSync({
        title: '@ionic-native/google-maps plugin!',
        snippet: 'This plugin is awesome!',
        position: location.latLng,
        animation: GoogleMapsAnimation.BOUNCE
      });

      // show the infoWindow
      marker.showInfoWindow();

      // If clicked it, display the alert
      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
        this.showToast('clicked!');
      });
    })
    .catch(err => {
      this.loading.dismiss();
      this.showToast(err.error_message);
    });
  }

  async showToast(message: string) {
    let toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'middle'
    });

    toast.present();
  }

  async modalLogin() {
    const modal = await this.modalController.create({
      component: LoginComponent,
    });
    return await modal.present();
  }

  async modalSignup() {
    const modal = await this.modalController.create({
      component: SignupComponent,
    });
    return await modal.present();
  }

  filter() {
    this.select.open();
  }

  logout() {
    this.auth.signOut();
  }
}
