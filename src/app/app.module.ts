import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Geolocation } from '@ionic-native/geolocation/ngx';

import { SignupComponent } from './components/signup/signup.component';
import { LoginComponent } from './components/login/login.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AuthService } from './services/auth/auth.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { AddPlaceComponent } from './components/add-place/add-place.component';
import { ShowPlaceComponent } from './components/show-place/show-place.component';
import { DenunciaComponent } from './components/denuncia/denuncia.component';

@NgModule({
  declarations: [AppComponent, LoginComponent, SignupComponent, AddPlaceComponent,DenunciaComponent, ShowPlaceComponent],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyC2zhfkilxgW4UW7Jxeu80VLnSeVv_0Kvk",
      authDomain: "leagueteam-4d06b.firebaseapp.com",
      databaseURL: "https://leagueteam-4d06b.firebaseio.com",
      projectId: "leagueteam-4d06b",
      storageBucket: "leagueteam-4d06b.appspot.com",
      messagingSenderId: "585608226363",
      appId: "1:585608226363:web:c05ef96aa37d25903a9e1e"
    }),
    IonicModule.forRoot(),
    AppRoutingModule
  ],
  entryComponents: [
    DenunciaComponent,
    LoginComponent,
    SignupComponent,
    AddPlaceComponent,
    ShowPlaceComponent
  ],
  providers: [
    Geolocation,
    AngularFireAuth,
    AngularFireDatabase,
    AuthService,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
