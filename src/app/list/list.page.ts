import { Component, OnInit } from '@angular/core';
import { GeofireService } from '../services/geofire/geofire.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Events, ModalController } from '@ionic/angular';
import { AuthService } from '../services/auth/auth.service';
import { ShowPlaceComponent } from '../components/show-place/show-place.component';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  places:Array<any> = [];
  position: any;

  constructor(public geo: GeofireService,
              public auth: AuthService,
              private geolocation: Geolocation,
              public modalController: ModalController,
              public events: Events) {
  }

  async ngOnInit() {
    await this.load();
  }
  async load() {
    const cords = await this.geolocation.getCurrentPosition();
    this.position = cords.coords;
    await this.geo.getLocations(100, [cords.coords.latitude, cords.coords.longitude]);
    if(this.auth.authenticated && this.auth.user) {
      if(this.auth.user.favoritos) {
        for (const key in this.auth.user.favoritos) {
          console.log(key);
          if(this.auth.user.favoritos[key]) {
            let place = await this.geo.getPlace(key);
            place['distancia'] = this.geo.calcDistance([this.position.latitude, this.position.longitude], place['l'])
            this.places.push(place);
          }          
        }
      }
    }
  }

  async showPlace(key) {
    const place = await this.geo.getPlace(key);
    place['distancia'] = this.geo.calcDistance([this.position.latitude, this.position.longitude], place['l'])    
    const modal = await this.modalController.create({
      component: ShowPlaceComponent,
      componentProps: {place},
    });
    await modal.present();
  }

  ngOnDestroy() {
  }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
