import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth/auth.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage {

  credentialsForm: FormGroup;
  infoForm: FormGroup;
  loginError: string;
  loading:Boolean = false;

  constructor(private auth: AuthService,
              private alertCtrl: AlertController,
              private fb: FormBuilder) { 
    this.setForm();
  }

  ionViewDidEnter() {
    this.patchForm();
  }

  setForm() {
		this.credentialsForm = this.fb.group({
			email: ['', Validators.compose([Validators.required, Validators.email])],
			password: ['', Validators.compose([Validators.minLength(6)])],
			confirm_password: ['', Validators.compose([Validators.minLength(6)])]
    });
    this.infoForm =this.fb.group({
			nome: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
			sobrenome: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
			apelido: ['', Validators.compose([Validators.required, Validators.minLength(1)])],
			veiculo: ['', Validators.compose([Validators.required])],
    });
  }

  matchPassword() {
    return this.credentialsForm.value.password == this.credentialsForm.value.confirm_password;
  }

  patchForm() {    
    this.credentialsForm.patchValue({email: this.auth.user.email});
    this.infoForm.patchValue({
      nome: this.auth.user.nome,
      sobrenome: this.auth.user.sobrenome,
      apelido: this.auth.user.apelido,
      veiculo: this.auth.user.veiculo,
    });
    
  
  }
  
  doUpdate(old_password = false) {
    let credentials = this.credentialsForm.value;
    let infoUser = this.infoForm.value;
    this.loading = true;

    if(credentials.password.length > 0 && !old_password) {
      return this.confirmChangePassword();
    }

		this.auth.updatePerfil(credentials, infoUser)
    .then( () => {})
    .catch( async error => {
      const alert = await this.alertCtrl.create({
        header: 'Erro!',
        message: error.message,
        buttons: ['OK']
      });
      alert.present();
    }).then(()=> this.loading = false);
  }

  async confirmChangePassword() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmar alteração',
      inputs: [
        {
          name: 'old_password',
          type: 'password',
          placeholder: 'Senha antiga'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Confirmar',
          handler: async alertData => {
            try {
              await this.auth.reAuth(alertData.old_password);
              this.doUpdate(true);
            } catch (error) {
              const alert = await this.alertCtrl.create({
                header: 'Erro!',
                message: error.message,
                buttons: ['OK']
              });
              alert.present();              
            }
          }
        }
      ]
    });

    await alert.present();
  }
}
