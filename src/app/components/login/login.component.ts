import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [AuthService]
})
export class LoginComponent implements OnInit {
  
	loginForm: FormGroup;
	loginError: string;

	constructor(private modalCtrl: ModalController,
				private auth: AuthService,
				private alertCtrl: AlertController,
				private fb: FormBuilder) { 
	this.setForm();
	}

	async setForm() {
		this.loginForm = this.fb.group({
			email: ['', Validators.compose([Validators.required, Validators.email])],
			password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
		});
	}

	doLogin() {

		let data = this.loginForm.value;

		if (!data.email) {
			return;
		}

		let credentials = {
			email: data.email,
			password: data.password
		};
		this.auth.login(credentials)
			.then( () => this.close())
			.catch( async error => {
				const alert = await this.alertCtrl.create({
					header: 'Erro!',
					message: error.message,
					buttons: ['OK']
				});
				await alert.present();
			});
	}

	close() {
	this.modalCtrl.dismiss();
	}

	ngOnInit() {}

}
