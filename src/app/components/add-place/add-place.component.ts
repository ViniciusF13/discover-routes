import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ModalController, ToastController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';
import { GeofireService } from 'src/app/services/geofire/geofire.service';

@Component({
  selector: 'app-add-place',
  templateUrl: './add-place.component.html',
  styleUrls: ['./add-place.component.scss'],
  providers: [AuthService,GeofireService]
})
export class AddPlaceComponent implements OnInit {

  form: FormGroup;
  error: string;
  coords;

  constructor(private modalCtrl: ModalController,
              private auth: AuthService,
              private geo: GeofireService,
              private toastController: ToastController,
              private fb: FormBuilder) { 
  this.setForm();
  }

  setForm() {
    
    this.form = this.fb.group({
      nome: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      tipo: ['', Validators.compose([Validators.required])],
      descricao: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  async cadastrar() {
    let data = this.form.value;
    data['user'] = this.auth.user.uid;
    const key = await this.geo.addPlace(data);
    this.geo.setLocation(key,[this.coords.lat, this.coords.lng]);
    const toast = await this.toastController.create({
      message: 'Local "'+ data['nome']+'" cadastrado com sucesso',
      animated: true,
      color: 'success',
      duration: 10000
      });
    toast.present();
    this.close();
  }

  close() {
    this.modalCtrl.dismiss();
  }

  ngOnInit() {}

}
