import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';
import { GeofireService } from 'src/app/services/geofire/geofire.service';
import { DenunciaComponent } from '../denuncia/denuncia.component';


@Component({
  selector: 'app-show-place',
  templateUrl: './show-place.component.html',
  styleUrls: ['./show-place.component.scss'],
  providers: [AuthService,GeofireService]
})
export class ShowPlaceComponent implements OnInit,AfterContentChecked {
  place:{key :String, tipo: String, distancia: any, denuncias: any, avaliacoes:any, nome: String, descricao: String} = null;
  nota = 0;
  novaNota = 0;
  mediaNota = [];
  mediaNotaVazio = [];

  constructor(private modalCtrl: ModalController,
              private geoService: GeofireService,
              public auth: AuthService) { }

  close() {
    this.modalCtrl.dismiss(this.place['denuncias'] && Object.keys(this.place['denuncias']).length == 3 ? true : false);
  }

  favoritar() {
    this.auth.addFavorito(this.place['key']);
  }

  avaliar(nota) {
    this.place = this.geoService.avaliarLocal(this.auth.user.uid, nota,this.place);
    this.nota = nota;
    this.novaNota = nota;
    
    this.mediaNota = Array(Math.round(this.place['media_nota'])).fill('');
    this.mediaNotaVazio = Array(5-Math.round(this.place['media_nota'])).fill('');
  }

  async denunciar() {
    const modal = await this.modalCtrl.create({
      component: DenunciaComponent,
    });
    await modal.present();
    let motivo = await modal.onDidDismiss();
    motivo = motivo.data;
    if(motivo) {
      this.place = this.geoService.denunciarLocal(this.auth.user.uid, motivo, this.place);
    }
  }

  ngAfterContentChecked() {    
    if(this.auth.authenticated) {
      this.nota = this.geoService.getAvaliacao(this.auth.user.uid, this.place);      
      this.novaNota = this.geoService.getAvaliacao(this.auth.user.uid, this.place);
    }    
  }
  ngOnInit() {
    console.log(this.place);
    
    this.mediaNota = Array(Math.round(this.place['media_nota'])).fill('');
    this.mediaNotaVazio = Array(5-Math.round(this.place['media_nota'])).fill('');
  }

}
