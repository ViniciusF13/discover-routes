import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  providers: [AuthService]
})
export class SignupComponent implements OnInit {
  
  signupForm: FormGroup;
  infoForm: FormGroup;
  loginError: string;
  
  constructor(private modalCtrl: ModalController,
		          private auth: AuthService,
              private alertCtrl: AlertController,
		          private fb: FormBuilder) { 
    this.setForm();
  }

  setForm() {
		this.signupForm = this.fb.group({
			email: ['', Validators.compose([Validators.required, Validators.email])],
			password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    });
    this.infoForm =this.fb.group({
			nome: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
			sobrenome: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
			apelido: ['', Validators.compose([Validators.required, Validators.minLength(1)])],
			veiculo: ['', Validators.compose([Validators.required])],
    });
  }
  
  doSignup() {
    let credentials = this.signupForm.value;
    let infoUser = this.infoForm.value;

		this.auth.signUp(credentials, infoUser)
    .then( () => this.close())
    .catch( async error => {
      const alert = await this.alertCtrl.create({
        header: 'Erro!',
        message: error.message,
        buttons: ['OK']
      });
      await alert.present();
    });
  }

  close() {
    this.modalCtrl.dismiss();
  }
  ngOnInit() {}

}
