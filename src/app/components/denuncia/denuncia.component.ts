import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-denuncia',
  templateUrl: './denuncia.component.html',
  styleUrls: ['./denuncia.component.scss'],
})
export class DenunciaComponent implements OnInit {
  motivo = '';

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {}

  close(save = false) {
    this.modalCtrl.dismiss(!save ? save : this.motivo);
  }

}
