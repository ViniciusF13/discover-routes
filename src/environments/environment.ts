// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyC2zhfkilxgW4UW7Jxeu80VLnSeVv_0Kvk",
    authDomain: "leagueteam-4d06b.firebaseapp.com",
    databaseURL: "https://leagueteam-4d06b.firebaseio.com",
    projectId: "leagueteam-4d06b",
    storageBucket: "leagueteam-4d06b.appspot.com",
    messagingSenderId: "585608226363",
    appId: "1:585608226363:web:c05ef96aa37d25903a9e1e"
	}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
